WITH CLIENTESSELECCIONADOS AS (
        SELECT ORD.EMPRESAID, ORD.CLIENTENROORD CLIENTENRO, MAX(ORD.ORDENNRO) ORDENNRO, max(PROMOTORID) PROMOTORID 
        FROM SIGATG.ORDENSRV ORD
                INNER JOIN SIGATG.CLIENTE CLI ON (ORD.EMPRESAID = CLI.EMPRESAID) AND (ORD.CLIENTENROORD = CLI.CLIENTENRO)
                INNER JOIN SIGATG.CONTRATO CO ON (CO.EMPRESAID = ORD.EMPRESAID) AND (CO.CONTRATONRO = ORD.CONTRATONRO)
        WHERE ORD.EMPRESAID = 10 AND CLI.CLIENTESTS = 'C' AND ORD.ORDENSTS = 'F' AND ORD.ORDENTPO = 'I' AND ORD.ORDENGEN = 'C' AND CLI.NEGOCIOSEGMENTO IN (2,4)
                AND ORD.ORDENFPROCESO >= TO_DATE('01/12/2018','DD/MM/YYYY')
                AND ORD.ORDENFPROCESO < TO_DATE('01/01/2019','DD/MM/YYYY')
                --AND ORD.CLIENTENROORD IN (11265514,11265550,11266189,10969651)
        GROUP BY ORD.EMPRESAID, ORD.CLIENTENROORD
), CLIENTECORREOS AS (
        SELECT CLIENTENRO, ROW_NUMBER() OVER (PARTITION BY CLIENTENRO ORDER BY CLIENTENRO, CLIENTEEMAILID) BID, CLIENTEEMAIL
        FROM SIGATG.CLIENTEEMAIL 
        WHERE EMPRESAID = 10
), ANTIGUEDAD_CLIENTE AS (
        SELECT CLIENTENRO, MIN(CASE WHEN CONTRATOFINS < TO_DATE('01/01/1980','DD/MM/YYYY') THEN SYSDATE ELSE CONTRATOFINS END)FECHAINSTALACION
        FROM SIGATG.CONTRATO 
        WHERE EMPRESAID = 10 AND CONTRATOSTS NOT IN ('A',' I', 'P',' E') --AND CLIENTENRO = 10738961
        GROUP BY CLIENTENRO
), RESUMEN_DATOS AS (
SELECT CASE WHEN E.LOGIN IS NULL THEN 
CASE WHEN SCC.SUBSEGMENTO IN ('LARGE','MEDIUM') THEN 'nlastra296' 
WHEN SCC.SUBSEGMENTO IN ('GOVERNMENT') THEN 'ahernandez117' 
WHEN SCC.SUBSEGMENTO IN ('PRIME') THEN 'ahernandez117' 
WHEN SCC.SUBSEGMENTO IN ('MICRO','SMALL','PYMES') THEN 'anaranjo111' END
ELSE E.LOGIN  END ID_EQUIPO_B2B,
                REPLACE(REPLACE(REPLACE( REPLACE(REPLACE( REPLACE(CLI.CLIENTENOM || ' ' || CLI.CLIENTEAPE,'"',' '),CHR(10),' '),CHR(13),' '), '|',' '),';',' '),',',' ') NOMBRE_CLIENTE,
                CCX.CLIENTENRO ID_CLIENTE,
                VCC1.CLIENTEEMAIL EMAIL_CLIENTE,
                to_char(ACL.FECHAINSTALACION,'MM/DD/YYYY HH24:MI:SS') FECHA_RENOVACION,
                '6' MERCADO,
                '2' MARCA,
                '1' CANAL,
                '2' SEGMENTO_CLIENTE,
                DECODE(CHV.HIGHTVALUE,'HIGH VALUE',1,2) TIPO_CLIENTE,
                '1' TIPO_TRANSACCION,
                CCX.ORDENNRO ID_TRANSACCION,
                to_char(trunc(ODN.ORDENFPROCESO),'MM/DD/YYYY HH24:MI:SS') FECHA_TRANSACCION,
                ODN.ORDGEODIV2 MUNICIPIO,
                DECODE(SCC.SUBSEGMENTO,'PYMES','1','MICRO','1','SMALL','2','MEDIUM','3','LARGE','4','GOVERNMENT','5','PRIME','6','WHOLESALE','7','') B2B_SEGMENTACION, 
                (CASE WHEN CLIENTEVINCULOPADRE <> 0 THEN 0 ELSE 1 END) MOSTRADO,
                CASE WHEN TO_CHAR(FECHAINSTALACION,'YYYYMM') = TO_CHAR( TO_DATE('01/11/2018','DD/MM/YYYY'),'YYYYMM') THEN 1 ELSE 2 END CLIENTE_NUEVO, 
                CASE WHEN TO_CHAR(FECHAINSTALACION,'YYYYMM') = TO_CHAR( TO_DATE('01/11/2018','DD/MM/YYYY'),'YYYYMM') THEN 2 ELSE 3 END  TIPO_COMPRA
        FROM CLIENTESSELECCIONADOS CCX
                LEFT JOIN SIGATG.CLIENTE CLI ON (CCX.EMPRESAID = CLI.EMPRESAID) AND (CCX.CLIENTENRO = CLI.CLIENTENRO)
                LEFT JOIN CLIENTECORREOS VCC1 ON (CCX.CLIENTENRO = VCC1.CLIENTENRO) AND (VCC1.BID = 1)
                LEFT JOIN ANTIGUEDAD_CLIENTE ACL ON (CCX.CLIENTENRO = ACL.CLIENTENRO)
                LEFT JOIN SERVICEREP.MEDALIA_CLIENTESHV CHV ON (CCX.CLIENTENRO = CHV.CLIENTENRO)
                LEFT JOIN SIGATG.ORDENSRV ODN ON (CCX.EMPRESAID = ODN.EMPRESAID) AND (CCX.ORDENNRO = ODN.ORDENNRO)
                LEFT JOIN SIGAMSATG.GEODIV2 GD2 ON ((GD2.PAISID = 10) AND (GD2.CIUDADID = ODN.ORDCIUDADID) AND (GD2.GEODIV1ID = ODN.ORDGEODIV1) AND (GD2.GEODIV2ID = ODN.ORDGEODIV2))
                LEFT JOIN SERVICEREP.MEDALIA_SUBCLASIFICACIONB2B SCC ON (CCX.CLIENTENRO = SCC.CLIENTENRO)
                LEFT JOIN MEDALIA_EJECUTIVOS_B2B E ON CCX.PROMOTORID = E.id_siga
)
SELECT ID_EQUIPO_B2B, TRIM(NOMBRE_CLIENTE) NOMBRE_CLIENTE, ID_CLIENTE, EMAIL_CLIENTE, FECHA_RENOVACION, MERCADO, MARCA, CANAL,SEGMENTO_CLIENTE, TIPO_CLIENTE,TIPO_TRANSACCION,ID_TRANSACCION,FECHA_TRANSACCION,MUNICIPIO,B2B_SEGMENTACION
,CLIENTE_NUEVO,TIPO_COMPRA
FROM RESUMEN_DATOS
WHERE MOSTRADO = 1
AND ID_CLIENTE NOT IN (SELECT DISTINCT CLIENTENRO FROM SERVICEREP.MEDALIA_CLIENTESB2B WHERE FECHA_REPORTE >= ADD_MONTHS(TRUNC(SYSDATE),-3))
AND B2B_SEGMENTACION IS NOT NULL
AND ID_EQUIPO_B2B IS NOT NULL;
