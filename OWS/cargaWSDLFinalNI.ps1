﻿function Invoke-SQL {
    param(
        [string] $dataSource = "10.7.57.97",
        [string] $database = "Replica_DWH",
        [string] $sqlCommand = ""
      )

    $connectionString = "Data Source=$dataSource; " +
            "user=sa;password=369Ad15min; " +
            "Initial Catalog=$database;" +
            "Connect Timeout=0" 

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()
    $command.CommandTimeout = 0
    #$adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $command.ExecuteNonQuery()

    $connection.Close()
    #ConvertTo-json($dataSet.Tables[0].Rows[0])
  
}

Invoke-SQL "10.7.57.97" "Replica_DWH" "truncate table [Replica_DWH].[OWS].[INCIDENTES_IN]"

$ts = New-TimeSpan -Start "01/01/2018" -End (Get-Date)
$dias = (($ts.Days)/7)+1 # Check results
$dias

$fechaInicio = [DateTime] "01/01/2018"
$fechafin =  ($fechaInicio).AddDays(7)

For($i=0; $i -le $dias; $i++) 
{
"ejecutando dia " + $fechaInicio + " al " + $fechafin 
#$fechafin.ToString("yyyy/MM/dd 00:00:00")
#$PSDefaultParameterValues['*:Encoding'] = 'utf8'

#Remove-Item "text.xml"

$soap = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wfm="http://wfm.huawei.com">
   <soapenv:Header>
      <wfm:authentication>
         <userName>powerbi</userName>
         <password>SDMtopowerBI2018.</password>
      </wfm:authentication>
   </soapenv:Header>
   <soapenv:Body>
      <wfm:GetList_inc>
         <interface>
            <start></start>
            <createtime_in>'+$fechaInicio.ToString("yyyy-MM-dd 00:00:00")+'</createtime_in>
            <createtime_fn>'+$fechafin.ToString("yyyy-MM-dd 00:00:00")+'</createtime_fn>
            <limit>1000</limit>
            <ticketid></ticketid>
            <orderid></orderid>
            <sort></sort>
            <asc></asc>
         </interface>
      </wfm:GetList_inc>
   </soapenv:Body>
</soapenv:Envelope>' 



$headers = @{ 
    'Content-Type' = 'text/xml;charset=utf-8'; 
    'SOAPAction' = 'GetList_inc'
    'Accept-Charset' = 'utf-8' 
}

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
$URI="https://sgintegration.teleows.com:8443/ws/services/106s/powerbi_sdm/getlist?wsdl"
$Proxy = New-WebserviceProxy $URI –Namespace X
#$Proxy | get-member -MemberType Method Get*

$secpasswd = ConvertTo-SecureString "SDMtopowerBI2018." -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("powerbi", $secpasswd)

#$soap = [System.Text.Encoding]::UTF8.GetBytes($soap);

Invoke-WebRequest -Uri $URI -Body $soap -Method Post -Credential $mycreds -Headers $headers -OutFile "textIN.xml"

[XML]$test = Get-Content -Path "textIN.xml"

"total de registros " + $test.Envelope.Body.GetList_incResponse.return.results.count
foreach($valor in  $test.Envelope.Body.GetList_incResponse.return.results){

$insert = "
INSERT INTO [OWS].[INCIDENTES_IN]
           ([orderid]
           ,[title]
           ,[createtime]
           ,[closetime]
           ,[et_inc]
           ,[ticketstatus]
           ,[assignto_inc]
           ,[originator]
           ,[priority_inc]
           ,[cm_fault_level]
           ,[workflowtype]
           ,[current_phase]
           ,[lastupdatetime]
           ,[na_inc]
           ,[at_inc_noc]
           ,[rc_inc]
           ,[hub_inc]
           ,[ap_inc]
           ,[enlace_inc]
           ,[tramo_fibra_inc]
           ,[node_inc]
           ,[ra_inc]
           ,[zone_inc]
           ,[responsibility_inc]
           ,[fot_inc]
           ,[op_inc]
           ,[cat_inc])
     VALUES
           ('"+$valor.orderid +"'
           ,'"+$valor.title +"'
           ,'"+$valor.createtime +"'
           ,'"+$valor.closetime +"'
           ,'"+$valor.et_inc +"'
           ,'"+$valor.ticketstatus +"'
           ,'"+$valor.assignto_inc +"'
           ,'"+$valor.originator +"'
           ,'"+$valor.priority_inc +"'
           ,'"+$valor.cm_fault_level +"'
           ,'"+$valor.workflowtype +"'
           ,'"+$valor.current_phase +"'
           ,'"+$valor.lastupdatetime +"'
           ,'"+$valor.na_inc +"'
           ,'"+$valor.at_inc_noc +"'
           ,'"+$valor.rc_inc +"'
           ,'"+$valor.hub_inc +"'
           ,'"+$valor.ap_inc +"'
           ,'"+$valor.enlace_inc +"'
           ,'"+$valor.tramo_fibra_inc +"'
           ,'"+$valor.node_inc +"'
           ,'"+$valor.ra_inc +"'
           ,'"+$valor.zone_inc +"'
           ,'"+$valor.responsibility_inc +"'
           ,'"+$valor.fot_inc +"'
           ,'"+$valor.op_inc +"'
           ,'"+$valor.cat_inc +"' )"

$registro= Invoke-SQL "10.7.57.97" "Replica_DWH" $insert

}

$fechafin =  ($fechafin).AddDays(7)
$fechaInicio =  ($fechaInicio).AddDays(7) 
}