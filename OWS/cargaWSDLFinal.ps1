﻿function Invoke-SQL {
    param(
        [string] $dataSource = "10.7.57.97",
        [string] $database = "Replica_DWH",
        [string] $sqlCommand = ""
      )

    $connectionString = "Data Source=$dataSource; " +
            "user=sa;password=369Ad15min; " +
            "Initial Catalog=$database;" +
            "Connect Timeout=0" 

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()
    $command.CommandTimeout = 0
    #$adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $command.ExecuteNonQuery()

    $connection.Close()
    #ConvertTo-json($dataSet.Tables[0].Rows[0])
  
}

Invoke-SQL "10.7.57.97" "Replica_DWH" "truncate table [OWS].[INCIDENTES_NH]"

$ts = New-TimeSpan -Start "01/01/2018" -End (Get-Date)
$dias = (($ts.Days)/7)+1  # Check results


$fechaInicio = [DateTime] "01/01/2018"
$fechafin =  ($fechaInicio).AddDays(7)

For($i=0; $i -le $dias; $i++) 
{
"ejecutando dia " + $fechaInicio + " al " + $fechafin 
#$fechafin.ToString("yyyy/MM/dd 00:00:00")
#$PSDefaultParameterValues['*:Encoding'] = 'utf8'

#Remove-Item "text.xml"

$soap = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wfm="http://wfm.huawei.com">
<soapenv:Header>
<wfm:authentication>
<userName>powerbi</userName>
<password>SDMtopowerBI2018.</password>
</wfm:authentication>
</soapenv:Header>
<soapenv:Body>
<wfm:GetList_inh> 
<interface> 
<creatime_in>'+$fechaInicio.ToString("yyyy-MM-dd 00:00:00")+'</creatime_in>
<creatime_fn>'+$fechafin.ToString("yyyy-MM-dd 00:00:00")+'</creatime_fn>
<start></start>
<limit>1000</limit>
<orderid></orderid>
<ticketid></ticketid>
<sort></sort>
<asc></asc>
</interface>
</wfm:GetList_inh>
</soapenv:Body>
</soapenv:Envelope>' 



$headers = @{ 
    'Content-Type' = 'text/xml;charset=utf-8'; 
    'SOAPAction' = 'GetList_inh'
    'Accept-Charset' = 'utf-8' 
}

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
$URI="https://sgintegration.teleows.com:8443/ws/services/106s/powerbi_sdm/getlist?wsdl"
$Proxy = New-WebserviceProxy $URI –Namespace X
#$Proxy | get-member -MemberType Method Get*

$secpasswd = ConvertTo-SecureString "SDMtopowerBI2018." -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("powerbi", $secpasswd)

#$soap = [System.Text.Encoding]::UTF8.GetBytes($soap);

Invoke-WebRequest -Uri $URI -Body $soap -Method Post -Credential $mycreds -Headers $headers -OutFile "text.xml"

[XML]$test = Get-Content -Path "text.xml"

$test.Envelope.Body.GetList_inhResponse.return.results.Count
foreach($valor in  $test.Envelope.Body.GetList_inhResponse.return.results){

$insert = "INSERT INTO [OWS].[INCIDENTES_NH]
           ([orderid]
           ,[createtime]
           ,[closetime]
           ,[title]
           ,[ticketstatus]
           ,[originator]
           ,[assing_to_ass]
           ,[inh_tipo_ttk]
           ,[inh_ti_cliente]
           ,[current_phase]
           ,[inh_seleccion_tiquete]
           ,[inh_descripcion_tiquete]
           ,[inh_responsability])
     VALUES
           ('"+$valor.orderid +"'
           ,'"+$valor.createtime +"'
           ,'"+$valor.closetime +"'
           ,'"+$valor.title +"'
           ,'"+$valor.ticketstatus +"'
           ,'"+$valor.originator +"'
           ,'"+$valor.assing_to_ass +"'
           ,'"+$valor.inh_tipo_ttk +"'
           ,'"+$valor.inh_ti_cliente +"'
           ,'"+$valor.current_phase +"'
           ,'"+$valor.inh_seleccion_tiquete +"'
           ,'"+$valor.inh_descripcion_tiquete +"'
           ,'"+$valor.inh_responsability +"')"

$registro= Invoke-SQL "10.7.57.97" "Replica_DWH" $insert

}
'fin dia ' + $i
$fechafin =  ($fechafin).AddDays(7)
$fechaInicio =  ($fechaInicio).AddDays(7) 
}