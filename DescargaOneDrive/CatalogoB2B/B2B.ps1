﻿#Add references to SharePoint client assemblies and authenticate to Office 365 site - required for CSOM
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"


function Invoke-SQL {
    param(
        [string] $dataSource = "10.7.57.97",
        [string] $database = "Replica_DWH",
        [string] $sqlCommand = ""
      )

    $connectionString = "Data Source=$dataSource; " +
            "user=sa;password=369Ad15min; " +
            "Initial Catalog=$database;" +
            "Connect Timeout=0" 

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()
    $command.CommandTimeout = 0
    #$adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $command.ExecuteNonQuery()

    $connection.Close()
    #ConvertTo-json($dataSet.Tables[0].Rows[0])
  
}


function Descarga-OneDrive {
    param(
        [string] $direccionOneDrive = "",
        [string] $destino = ""
      )

$siteUrl = "https://millicom-my.sharepoint.com/personal/cubosql_tigo_co_cr/" #DWHTIGO/
$listTitle = "Documents"
$destination = $destino

# Login as the user account to window server and run the following script
# read-host -AsSecureString | ConvertFrom-SecureString | out-file C:\cred.txt 
# The password will be encripted to file 
$o365admin = "cubosql@tigo.co.cr"
$Pass = "Qoqa6629"
$SecurePassword = $Pass | ConvertTo-SecureString -AsPlainText -Force
#$password = $pa | convertto-securestring
$credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($o365admin,$SecurePassword)

$ctx = New-Object Microsoft.SharePoint.Client.ClientContext($siteUrl)
$ctx.Credentials = $credentials

#Load items
$list = $ctx.Web.Lists.GetByTitle($listTitle)
$query = [Microsoft.SharePoint.Client.CamlQuery]::CreateAllItemsQuery()
$items = $list.GetItems($query)
$ctx.Load($list)
$ctx.Load($items)
$ctx.ExecuteQuery()
$items.Count

foreach ($File in $items | Where-Object {$($_["FileRef"]) -like $direccionOneDrive } ) {
         #Write-host "Url: $($File["FileRef"]), title: $($File["FileLeafRef"]) "
        #if($($File["FileRef"]) -like "/personal/cubosql_tigo_co_cr/Documents/CatalogoB2B*"){
         Write-host $($File["FileRef"])
        if($File.FileSystemObjectType -eq [Microsoft.SharePoint.Client.FileSystemObjectType ]::File) { 

            $fileRef = $($File["FileRef"]).ToString()
            #$fileRef

            $fileInfo = [Microsoft.SharePoint.Client.File]::OpenBinaryDirect($ctx, $fileRef);
            $fileName = $destination + "\\" + $File["FileLeafRef"]
            $fileStream = [System.IO.File]::Create($fileName)
            $fileInfo.Stream.CopyTo($fileStream);
            $fileStream.Close()

        }
        #}
}

} #fin funcion 

Descarga-OneDrive "/personal/cubosql_tigo_co_cr/Documents/CatalogoB2B/*"  "C:\temp"


$prod = Import-Csv -Path "C:\temp\PRODUCTOS_B2B_PYMES.csv" -Delimiter ","

Invoke-SQL "10.7.57.97" "Replica_DWH" "delete from [CARGAS].[PRODUCTOS_B2B_PYMES]"

$insert = "INSERT INTO [CARGAS].[PRODUCTOS_B2B_PYMES]
           ([IDPRODUCTO]
           ,[FAMILIA]
           ,[SUBFAMILIA]
           ,[NOMBREPRODUCTO]
           ,[RGU]
           ,[FORMAPAGO]
           ,[SEGMENTO]
           ,[TECNOLOGIA]
           ,[PRIMER_NIVEL]
           ,[SEGUNDO_NIVEL]
           ,[TERCEL_NIVEL]
           ,[CUARTO_NIVEL])
     VALUES
           ("

foreach($p in  $prod){


$ejecutar = $insert + $p.IDPRODUCTO+ ", '"+ $p.FAMILIA +"'" +",'"+ $p.SUBFAMILIA + "'" +",'"+ $p.NOMBREPRODUCTO + "'" +",'"+ $p.RGU + "'" +",'"+ $p.FORMAPAGO + "'" +",'"+ $p.SEGMENTO + "'"  +",'"+ $p.TECNOLOGIA + "'" +",'"+ $p.PRIMER_NIVEL + "'" +",'"+ $p.SEGUNDO_NIVE + "'" +",'"+ $p.TERCEL_NIVEL + "'"   +",'"+ $p.CUARTO_NIVEL + "')" 
        

Invoke-SQL "10.7.57.97" "Replica_DWH" $ejecutar
}
#PRODUCTOS_B2B_PYMES

  
           
           
           
           
           
           
           
            