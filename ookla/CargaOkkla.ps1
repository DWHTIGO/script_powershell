﻿Clear-Host 
Write-Host "Comenzando Proceso de Descarga de Archivos...."
$user = "millicom_international_cellular"#Nombre de Usuario
$pass=  ConvertTo-SecureString "sUCC2f5e" -AsPlainText -Force #Contraseña Incriptada
$cred = New-Object System.Management.Automation.PSCredential ($user, $pass)# Se nos solicita el Usuario y Contraseña para ingresar al archivo ftp
$Site ="http://files.speedtest.ookla.com/millicom_international_cellular/v2/" #Sitio donde vamos a descargar lo que hay en una Carpeta, por lo cual el ultimo nombre seria el de la carpeta
$Test =Invoke-WebRequest -URI $Site -Credential $cred
$Test.Links | Foreach {
#-------------------------------------------------------------------------Comienzo de STdesktop
if($a-match ("STdesktop_(?<content>.*).zip")){ 
#$matches['content'] ---está variable nos muestras las fechas dentro del contenido "STdesktop_(?<content>.*).zip" 
$datos=(Get-Date -Format $matches['content'] )
#$datos----#Nos muestra las Fechas cacturadas en String y las convierte en formato de fecha
$fecha= Get-Date # Captura la fecha del día en que estamos
$ts = New-TimeSpan -Start $datos -End $fecha #Condicion de fecha optenida y determinamos fin de fecha en que estamos
#$ts.Days ---# Da la cantidad de dias de lOS DATOS(FECHAS) A FECHA DE HOY
      if($ts.Days -lt 30){
      $site+$a
      $datos 
      $Descarga=$site+$a
      $Test =Invoke-WebRequest -URI $Descarga -OutFile "C:\ProcesosCargas\okkla\files\$a" -Credential $cred     #SE DEBE CAMBIAR LA RUTA DONDE SE VA A DESCARGAR LOS ARCHIVOS
#Para descomprimir el Archivo .Zip
Set-Location "C:\ProcesosCargas\okkla\files" 
$Unzip = New-Object -ComObject Shell.Application
$FileName = $a
$ZipFile = $Unzip.NameSpace((Get-Location).Path + "\$FileName") 
$Destination = $Unzip.namespace((Get-Location).Path) 
$Destination.Copyhere($ZipFile.items())
      }#Final de If
}#Final de If
#------------------------------------------------------------------------------Continua Android
if($a-match ("android_(?<content>.*).zip")){
$datos=(Get-Date -Format $matches['content'] )
#$datos
$fecha= Get-Date
$ts = New-TimeSpan -Start $datos -End $fecha 
#$ts.Days---- # Da la cantidad de dias de lOS DATOS(FECHAS) A FECHA DE HOY
      if($ts.Days -lt 30){
      $site+$a
      $datos 
      $Descarga=$site+$a
      $Test =Invoke-WebRequest -URI $Descarga -OutFile "C:\ProcesosCargas\okkla\files\$a" -Credential $cred      #SE DEBE CAMBIAR LA RUTA DONDE SE VA A DESCARGAR LOS ARCHIVOS

#Para descomprimir el Archivo .Zip
Set-Location "C:\ProcesosCargas\okkla\files" 
$Unzip = New-Object -ComObject Shell.Application
$FileName = $a
$ZipFile = $Unzip.NameSpace((Get-Location).Path + "\$FileName") 
$Destination = $Unzip.namespace((Get-Location).Path) 
$Destination.Copyhere($ZipFile.items())
      }#Final de If
}
#----------------------------------------------------------------------Continua iOs
if($a-match ("iOs_(?<content>.*).zip")){
$datos=(Get-Date -Format $matches['content'] )
#$datos
$fecha= Get-Date
$ts = New-TimeSpan -Start $datos -End $fecha 
#$ts.Days # Da la cantidad de dias de lOS DATOS(FECHAS) A FECHA DE HOY
      if($ts.Days -lt 30){
      $site+$a
      $datos 
      $Descarga=$site+$a
      $Test =Invoke-WebRequest -URI $Descarga -OutFile "C:\ProcesosCargas\okkla\files\$a" -Credential $cred         #SE DEBE CAMBIAR LA RUTA DONDE SE VA A DESCARGAR LOS ARCHIVOS

#Para descomprimir el Archivo .Zip
Set-Location "C:\ProcesosCargas\okkla\files" 
$Unzip = New-Object -ComObject Shell.Application
$FileName = $a
$ZipFile = $Unzip.NameSpace((Get-Location).Path + "\$FileName") 
$Destination = $Unzip.namespace((Get-Location).Path) 
$Destination.Copyhere($ZipFile.items())
      }#Final de If
}
#----------------------------------------------------------------------Continua Stnet
if($a-match ("stnet_(?<content>.*).zip")){
$datos=(Get-Date -Format $matches['content'] )
#$datos
$fecha= Get-Date
$ts = New-TimeSpan -Start $datos -End $fecha 
#$ts.Days # Da la cantidad de dias de lOS DATOS(FECHAS) A FECHA DE HOY
      if($ts.Days -lt 30){
      $site+$a
      $datos 
      $Descarga=$site+$a
      $Test =Invoke-WebRequest -URI $Descarga -OutFile "C:\ProcesosCargas\okkla\files\$a" -Credential $cred        #SE DEBE CAMBIAR LA RUTA DONDE SE VA A DESCARGAR LOS ARCHIVOS

#Para descomprimir el Archivo .Zip
Set-Location "C:\ProcesosCargas\okkla\files" 
$Unzip = New-Object -ComObject Shell.Application
$FileName = $a
$ZipFile = $Unzip.NameSpace((Get-Location).Path + "\$FileName") 
$Destination = $Unzip.namespace((Get-Location).Path) 
$Destination.Copyhere($ZipFile.items())
      }#Final de If
}
#----------------------------------------------------------------------Continua WP
if($a-match ("wp_(?<content>.*).zip")){
$datos=(Get-Date -Format $matches['content'] )
#$datos
$fecha= Get-Date
$ts = New-TimeSpan -Start $datos -End $fecha 
#$ts.Days # Da la cantidad de dias de lOS DATOS(FECHAS) A FECHA DE HOY
      if($ts.Days -lt 30){
      $site+$a
      $datos 
      $Descarga=$site+$a
      $Test =Invoke-WebRequest -URI $Descarga -OutFile "C:\ProcesosCargas\okkla\files\$a" -Credential $cred          #SE DEBE CAMBIAR LA RUTA DONDE SE VA A DESCARGAR LOS ARCHIVOS

#Para descomprimir el Archivo .Zip
Set-Location "C:\ProcesosCargas\okkla\files" 
$Unzip = New-Object -ComObject Shell.Application
$FileName = $a
$ZipFile = $Unzip.NameSpace((Get-Location).Path + "\$FileName") 
$Destination = $Unzip.namespace((Get-Location).Path) 
$Destination.Copyhere($ZipFile.items())
      }#Final de If
}
$a=$_.Href
}#Final de ForEach
Write-Host "SE han descargado los archivos Zip y se han Descomprimido..."
#------------------------------------------------------------------------------------------------------------------------
Write-Host "Empezando Proceso de Carga de archivos...."
write-Host "Cargando archivos a la Base de Datos...."
#Llamar los Archivos descargados desde la carpeta y Subirlos a la base de datos deacuerdo a su respectiva tabla
$P = New-Object system.Data.DataTable 
$P=Get-ChildItem -Path "C:\ProcesosCargas\okkla\files" -Recurse -Include *.csv         #--------------------------------------------------#SE DEBE CAMBIAR LA RUTA DONDE SE VA SE CARGA EL Archivo

$FileExists = Test-Path $P
If ($FileExists -eq $True) {Write-Host "Buscando los archivos csv..."
$ruta=Resolve-Path($P) #Se pasa a Resolve-Path para Obtener la ruta
} 
Else {Write-Host "No se encuentra el archivo...Error"}
#$ruta   #Imprime rutas de archivos dentro de la Carpeta( En este caso solo .csv)
#Se crea la tabla y se manda a llamar las rutas con sus respectivas condiciones
# Se crea la Conexion con la base de datos
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection ("Data Source = 192.168.10.36; User=sa;Password=369Ad15min; Initial Catalog = Seguimiento");
$sqlConnection.Open()
#$P    # imprime $P para verificar que si esta Opteniendo los datos q tengas las condiciones q solicitamos
#Funcion que elimina e inserta los datos actualizados
function ExecuteSql([Data.SqlClient.SqlConnection] $OpenSQLConnection,$datos){
$sqlCommand = New-Object System.Data.SqlClient.SqlCommand
$sqlCommand.Connection = $sqlConnection
$sqlCommand.CommandText = $datos
$InsertedID = $sqlCommand.ExecuteScalar()
}
ExecuteSql $sqlConnection "Delete from dbo.TEMP_OOKLA_HISTORICO_ANDROID"
ExecuteSql $sqlConnection "Delete from dbo.TEMP_OOKLA_HISTORICO_IOS"
ExecuteSql $sqlConnection "Delete from dbo.TEMP_OOKLA_HISTORICO_V2"

if($ruta= Get-ChildItem -Path "C:\ProcesosCargas\okkla\files" -Recurse -Include iOS*.csv){                 #--------------------------------------------------#SE DEBE CAMBIAR LA RUTA DONDE SE VA SE CARGA EL Archivo
Write-Host "Hay archivos iOs..."
$ruta
$P=Import-Csv $ruta | ? isp_name -eq 'Millicom Cable Costa Rica S.A.'
$insert= "Insert into dbo.TEMP_OOKLA_HISTORICO_IOS values("
foreach($file in $P){
$insert2=$insert+$file.test_id+","+
$file.iphone_device_id+",'"+
$file.test_date+"','"+
$file.client_ip_address_s+"','"+
$file.download_kbps+"','"+
$file.upload_kbps+"','"+
$file.latency+"','"+
$file.server_name+"','"+
$file.server_country+"','"+
$file.server_country_code+"','"+
$file.server_latitude+"','"+
$file.server_longitude+"','"+
$file.server_sponsor_name+"','"+
$file.client_country+"','"+
$file.client_country_code+"','"+
$file.client_region_name+"','"+
$file.client_region_code+"','"+
$file.client_city+"','"+
$file.client_latitude+"','"+
$file.client_longitude+"','"+
$file.miles_between+"','"+
$file.pre_connection_type+"','"+
$file.post_connection_type+"','"+
$file.isp_name+"','"+
$file.is_isp+"','"+
$file.network_operator_name+"','"+
$file.iso_country_code+"','"+
$file.mcc+"','"+
$file.mnc+"','"+
$file.model+"','"+
$file.version+"','"+
$file.location_type+"','"+
$file.gmaps_formatted_address+"','"+
$file.gmaps_name+"','"+
$file.gmaps_type+"','"+
$file.gmaps_country+"','"+
$file.gmaps_country_code+"','"+
$file.gmaps_region+"','"+
$file.gmaps_subregion+"','"+
$file.gmaps_subsubregion+"','"+
$file.gmaps_postal_code+"','"+
$file.pre_device_ip_address_a+"','"+
$file.post_device_ip_address_a+"','"+
$file.app_version_a+"','"+
$file.altitude_a+"','"+
$file.timezone_name_a+"','"+
$file.timezone_offset_a+"','"+
$file.test_method_a+"','"+
$file.ploss_sent_a+"','"+
$file.ploss_recv_a+"','"+
$file.jitter_a+"','"+
$file.wifi_bssid_a+"','"+
$file.tr_ip_0_a+"','"+
$file.tr_latency_a+"','"+
$file.tr_ip_1_a+"','"+
$file.tr_latency_1_a+"'"+")"
ExecuteSql $sqlConnection $insert2
}#final de ForEach
}#final if

if($ruta= Get-ChildItem -Path "C:\ProcesosCargas\okkla\files" -Recurse -Include android*.csv){                          #--------------------------------------------------#SE DEBE CAMBIAR LA RUTA DONDE SE VA SE CARGA EL Archivo
Write-Host "Hay archivos Android..."
$ruta
$P=Import-Csv $ruta | ? isp_name -eq 'Millicom Cable Costa Rica S.A.'
$insert= "Insert into dbo.TEMP_OOKLA_HISTORICO_ANDROID values("
foreach($file in $P){
$insert2=$insert+$file.test_id+","+
$file.android_device_id+",'"+
$file.android_fingerprint+"','"+
$file.test_date+"','"+
$file.client_ip_address+"',"+
$file.download_kbps+","+
$file.upload_kbps+","+
$file.latency+",'"+
$file.server_name+"','"+
$file.server_country+"','"+
$file.server_country_code+"',"+
$file.server_latitude+","+
$file.server_longitude+",'"+
$file.server_sponsor_name+"','"+
$file.client_country+"','"+
$file.client_country_code+"','"+
$file.client_region_name+"','"+
$file.client_region_code+"','"+
$file.client_city+"',"+
$file.client_latitude+","+
$file.client_longitude+",'"+
$file.miles_between+"','"+
$file.isp_name+"','"+
$file.is_isp+"','"+
$file.network_operator_name+"','"+
$file.mcc+"','"+
$file.mnc+"','"+
$file.pre_connection_type+"','"+
$file.post_connection_type+"','"+
$file.brand+"','"+
$file.device+"','"+
$file.hardware+"','"+
$file.build_id+"','"+
$file.manufacturer+"','"+
$file.model+"','"+
$file.product+"','"+
$file.cdma_cell_id+"','"+
$file.gsm_cell_id+"','"+
$file.location_type+"','"+
$file.gmaps_formatted_address+"','"+
$file.gmaps_name+"','"+
$file.gmaps_type+"','"+
$file.gmaps_country+"','"+
$file.gmaps_country_code+"','"+
$file.gmaps_region+"','"+
$file.gmaps_subregion+"','"+
$file.gmaps_subsubregion+"','"+
$file.gmaps_postal_code+"','"+
$file.phone_type_a+"','"+
$file.device_software_version_a+"','"+
$file.sim_network_operator_name_a+"','"+
$file.sim_network_operator_code_a+"','"+
$file.app_version_a+"','"+
$file.start_cell_id_a+"','"+
$file.wifi_bssid_a+"','"+
$file.wifi_secure_a+"','"+
$file.wifi_rssi_a+"','"+
$file.alt_sim_network_operator_name_a+"','"+
$file.alt_sim_network_operator_code_a+"','"+
$file.test_method_a+"','"+
$file.gsm_lac_a+"','"+
$file.alt_sim_operator_alpha_long_a+"','"+
$file.timezone_name_a+"','"+
$file.timezone_offset_seconds_a+"','"+
$file.data_connection_type_a+"','"+
$file.android_api_a+"','"+
$file.architecture_a+"','"+
$file.signal_cell_type_a+"','"+
$file.pci_a+"','"+
$file.tac_a+"','"+
$file.base_station_id_a+"','"+
$file.station_latitude_a+"','"+
$file.station_longitude_a+"','"+
$file.network_id_a+"','"+
$file.system_id_a+"','"+
$file.cid_a+"','"+
$file.lac_a+"','"+
$file.psc_a+"','"+
$file.asu_level_a+"','"+
$file.dbm_a+"','"+
$file.level_a+"','"+
$file.timing_advance_a+"','"+
$file.cdma_dbm_a+"','"+
$file.cdma_ecio_a+"','"+
$file.cdma_level_a+"','"+
$file.evdo_dbm_a+"','"+
$file.evdo_ecio_a+"','"+
$file.evdo_level_a+"','"+
$file.evdo_snr_a+"','"+
$file.signal_string_a+"','"+
$file.jitter_a+"','"+
$file.ploss_sent_a+"','"+
$file.ploss_recv_a+"','"+
$file.valid_imei_a+"','"+
$file.app_store_a+"','"+
$file.is_rooted_a+"','"+
$file.tr_ip_0_a+"','"+
$file.tr_latency_a+"','"+
$file.tr_ip_1_a+"','"+
$file.tr_latency_1_a+"','"+
$file.wifi_speed_mbps_a+"','"+
$file.wifi_frequency_mhz_a+"','"+
$file.location_accuracy_a+"','"+
$file.uarfcn_a+"','"+
$file.arfcn_a+"','"+
$file.bsic_a+"','"+
$file.earfcn_a+"','"+
$file.subscription_code_a+"','"+
$file.test_carrier_a+"','"+
$file.test_mcc_a+"','"+
$file.test_mnc_a+"','"+
$file.server_selection_a+"','"+
$file.rsrp_a+"','"+
$file.rsrq_a+"','"+
$file.rssnr_a+"','"+
$file.cqi_a+"'"+ ")"
ExecuteSql $sqlConnection $insert2
}#final de ForEach
}#Final If

if($ruta= Get-ChildItem -Path "C:\ProcesosCargas\okkla\files" -Recurse -Include stnet*.csv){            #--------------------------------------------------#SE DEBE CAMBIAR LA RUTA DONDE SE VA SE CARGA EL Archivo
Write-Host "Hay archivos stnet..."
$ruta
$P=Import-Csv $ruta | ? isp_name -eq 'Millicom Cable Costa Rica S.A.'
$insert= "Insert into dbo.TEMP_OOKLA_HISTORICO_V2  values("
foreach($file in $P){
$insert2=$insert+$file.test_id+",'"+
$file.test_date+"','"+
$file.client_ip_id+"','"+
$file.client_ip_address+"',"+
$file.download_kbps+","+
$file.upload_kbps+","+
$file.latency+",'"+
$file.server_name+"','"+
$file.server_country+"','"+
$file.server_country_code+"',"+
$file.server_latitude+","+
$file.server_longitude+",'"+
$file.server_sponsor_name+"','"+
$file.user_agent+"','"+
$file.isp_name+"','"+
$file.is_isp+"','"+
$file.connection_type+"','"+
$file.client_country+"','"+
$file.client_country_code+"','"+
$file.client_region_name+"','"+
$file.client_region_code+"','"+
$file.client_city+"',"+
$file.client_latitude+","+
$file.client_longitude+",'"+
$file.miles_between+"','"+
$file.gmaps_formatted_address+"','"+
$file.gmaps_name+"','"+
$file.gmaps_type+"','"+
$file.gmaps_country+"','"+
$file.gmaps_country_code+"','"+
$file.gmaps_region+"','"+
$file.gmaps_subregion+"','"+
$file.gmaps_subsubregion+"','"+
$file.gmaps_postal_code+"','"+
$file.session_id_a+"','"+
$file.test_method_a+"','"+
$file.test_source_a+"','"+
$file.client_ipv6_address_s+"'"+ ")" 
ExecuteSql $sqlConnection $insert2
}#final de ForEach
}#Final If
Write-Host "Proceso Finalizado Exitosamente..."
#----------------------------------------------------------------------------------------------------------- 

ExecuteSql $sqlConnection "execute [dbo].[Carga_Historico_OOKLA]"

ExecuteSql $sqlConnection "execute [dbo].[Carga_Concilacion_Test_OOKLA]"